const professorService = {
  professorLists: [
    // {
    //   id: 1,
    //   studentId: '60160065',
    //   name: 'Phatcharapol Saenrang',
    //   professor: 'อาจารย์กบ อ๊บๆ'
    // },
    // {
    //   id: 2,
    //   studentId: '60160077',
    //   name: 'Sompong Prera',
    //   professor: 'อาจารย์เอก'
    // }
  ],
  lastId: 3,
  addProfessor (professor) {
    professor.id = this.lastId++
    this.professorLists.push(professor)
  },
  updateProfessor (professor) {
    const index = this.professorLists.findIndex(item => item.id === professor.id)
    this.professorLists.splice(index, 1, professor)
  },
  deleteProfessor (professor) {
    const index = this.professorLists.findIndex(item => item.id === professor.id)
    this.professorLists.splice(index, 1)
  },
  getProfessors () {
    return [...this.professorLists]
  }
}

export default professorService
