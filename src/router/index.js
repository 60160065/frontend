import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'addprofessor',
    component: () => import(/* webpackChunkName: "about" */ '../views/addProfessor.vue')
  },
  {
    path: '/addprofessor',
    name: 'addprofessor',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/addProfessor.vue')
  },
  {
    path: '/checkstatus',
    name: 'checkstatus',
    component: () => import(/* webpackChunkName: "about" */ '../views/checkStatus.vue')
  },
  {
    path: '/checkhour',
    name: 'checkhour',
    component: () => import(/* webpackChunkName: "about" */ '../views/checkHour.vue')
  },
  {
    path: '/checkgrade',
    name: 'checkgrade',
    component: () => import(/* webpackChunkName: "about" */ '../views/checkGrade.vue')
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
